from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Peter'  # TODO Implement this
last_name = 'Wenji'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 8, 18)  # TODO Implement this, format (Year, Month, Date)
npm = 2017130003  # TODO Implement this
asalSekolah = 'STMIK LIKMI'
asalDaerah = 'Bandung'
tempatLahir = 'Bandung'


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'lname': last_name, 'age': calculate_age(birth_date.year), 'npm': npm,
                'asalSekolah': asalSekolah, 'asalDaerah': asalDaerah, 'tempatLahir': tempatLahir,
                'birthdate': str(birth_date.strftime("%d, %B %Y"))}
    return render(request, 'profil.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
